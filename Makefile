# Makefile for BootCommands
#
# ***********************************
# ***    C h a n g e   L i s t    ***
# ***********************************
# Date           Name       Description
# ----           ----       -----------
# 5 Oct 2010     TM         Created.
#

#
# Generic options:
#
MKDIR   = do mkdir -p
CC      = cc -IC:,OSLib:,USBLib: -fach -wp
CP      = copy
CPFLAGS = ~cfr~v
OBJASM  = ObjAsm ${THROWBACK} -depend !Depend -stamp -quit
CMHG    = CMHG -IC:,OSLiB:,USBLib: -d $(subst .o.,.h.,@.o.ftdimodhead)
LINK    = Link 
ZM      = -zM
WIPE    = -wipe
XWIPE   = x wipe
WFLAGS  = ~c~vr
RM      = remove

STUBS   = CLib:o.Stubs

#
# Program specific options:
#
COMPONENT = FTDI
APP        = !FTDI
MODULE     = rm.${COMPONENT}
RDIR       = Resources
LDIR       = ${RDIR}.${LOCALE}

#
# Export Paths for Messages module
#
RESDIR = <resource$dir>.Resources.${COMPONENT}
RESAPP = <resource$dir>.Apps.${APP}

#
# Libraries:
#
OSLIB  = OSLib:OSLib.o
USBLib = USBLib:USBLIB.o

FILES =\
 Docs.Manual.*\
 ${RDIR}.!Boot\
 ${RDIR}.!Help\
 ${RDIR}.!Run\
 ${RDIR}.Devices.*\
 ${RDIR}.Help\
 ${RDIR}.NewDevs\
 ${RDIR}.SetAlias\
 ${RDIR}.StrtSmDevs\
 $(LDIR).*\
 RM.FTDI

OBJS = o.error o.ftdi o.configuration o.serial \
       o.upcall o.ftdimodule o.callback o.debug \
       o.driver o.command o.ftdimodhead

#
# Generic rules {used by top-level build}:
#

all: rm.${COMPONENT}

install: ${FILES}
	${MKDIR} ${INSTDIR}
	${MKDIR} ${INSTDIR}.Docs.Manual
	${MKDIR} ${INSTDIR}.Devices
	${MKDIR} ${INSTDIR}.$(LDIR)
	$(CP) Docs.Manual.*      ${INSTDIR}.Docs.Manual.* ${CPFLAGS}
	$(CP) $(RDIR).!Boot      ${INSTDIR}.!Boot         ${CPFLAGS}
	$(CP) $(RDIR).!Help      ${INSTDIR}.!Help         ${CPFLAGS}
	$(CP) $(RDIR).!Run       ${INSTDIR}.!Run          ${CPFLAGS}
	$(CP) $(RDIR).Devices.*  ${INSTDIR}.Devices.*     ${CPFLAGS}
	$(CP) $(RDIR).Help       ${INSTDIR}.Help          ${CPFLAGS}
	$(CP) $(RDIR).NewDevs    ${INSTDIR}.NewDevs       ${CPFLAGS}
	$(CP) $(RDIR).SetAlias   ${INSTDIR}.SetAlias      ${CPFLAGS}
	$(CP) $(RDIR).StrtSmDevs ${INSTDIR}.StrtSmDevs    ${CPFLAGS}
	$(CP) $(LDIR).*          ${INSTDIR}.$(LDIR).*     ${CPFLAGS}
	$(CP) RM.FTDI            ${INSTDIR}.FTDI          ${CPFLAGS}
	-Access ${INSTDIR}.Docs.Manual.* lr/r
	-Access ${INSTDIR}.!Boot         lr/r
	-Access ${INSTDIR}.!Help         lr/r
	-Access ${INSTDIR}.!Run          lr/r
	-Access ${INSTDIR}.Devices.*     lr/r
	-Access ${INSTDIR}.FTDI          lr/r
	-Access ${INSTDIR}.Help          lr/r
	-Access ${INSTDIR}.NewDevs       lr/r
	-Access ${INSTDIR}.SetAlias      lr/r
	-Access ${INSTDIR}.StrtSmDevs    lr/r
	-Access ${INSTDIR}.$(LDIR).*     lr/r
	@echo ${COMPONENT}: Application installed (Disc)

clean:
	${XWIPE} o.* ${WFLAGS}
	${XWIPE} rm.* ${WFLAGS}
	@echo ${COMPONENT}: cleaned

#Internal targets
RM.FTDI: ${OBJS} ${OSLIB} ${STUBS}
	${LINK} -rmf -output RM.FTDI ${OBJS} ${OSLIB} ${STUBS}
	Access RM.FTDI WR/R

#General rules
.SUFFIXES:   .cmhg .c .debug .o .asm .def
.c.o:;       ${CC} ${THROWBACK} -depend !Depend -c ${ZM} -ff $<
.asm.o:;     ${OBJASM} -stamp -quit $< $@
.asm.debug:; ${OBJASM} -pd "TRACE SETL {TRUE}" -pd "STANDALONE SETL {TRUE}" -stamp -quit $< $@
.c.debug:;   ${CC} ${THROWBACK} -depend !Depend -c ${ZM} -ff -DTRACE=1 -DSTANDALONE -o $@ $<
.cmhg.o:;    ${CMHG} -p -depend !Depend $< $@
.c.s:;       ${CC} ${THROWBACK} -depend !Depend -s ${ZM} -ff $<
.def.o:;     DefMod -l -o l.$* < $<
	     LibFile -c -o $@ -via ViaFile
	     Wipe l.$* ~C~FR~V
.def.h:;     DefMod -h < $< > $@
.def.s:;     DefMod -s < $< > $@

# Dynamic dependencies:
